package vn.hesty.plugin.r2dbcmodelgen;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.TypeParameterElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import javax.tools.JavaFileObject;

import org.springframework.data.annotation.Transient;
import vn.hesty.plugin.r2dbcmodelgen.AnnotationProcessorUtils.TypeCategory;
import vn.hesty.plugin.r2dbcmodelgen.anotation.SuperTable;
import vn.hesty.plugin.r2dbcmodelgen.utils.Constants;
import vn.hesty.plugin.r2dbcmodelgen.utils.TypeUtils;

@SupportedAnnotationTypes({"org.springframework.data.relational.core.mapping.Table"})
public class CriteriaProcessor extends AbstractProcessor
{
    private static final String CLASS_NAME_SUFFIX = "_";

    private static final String CODE_INDENT = "    ";

    Types typesHandler;

    private boolean isR2DBCEntity(Element element) {
        return TypeUtils.containsAnnotation(
                element,
                Constants.ENTITY,
                Constants.SUPER_ENTITY
        );
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv)
    {
        if (roundEnv.processingOver())
        {
            return false;
        }

        typesHandler = processingEnv.getTypeUtils();
        Set<? extends Element> elements = roundEnv.getRootElements();
        for (Element e : elements)
        {
            if (e instanceof TypeElement && isR2DBCEntity(e))
            {
                processClass((TypeElement)e);
            }
        }
        return false;
    }

    /**
     * Handler for processing a JPA annotated class to create the criteria class stub.
     * @param el The class element
     */
    protected void processClass(TypeElement el)
    {
        if (el == null)
        {
            return;
        }

        Elements elementUtils = processingEnv.getElementUtils();
        String className = elementUtils.getBinaryName(el).toString();
        String pkgName = className.substring(0, className.lastIndexOf('.'));
        String classSimpleName = className.substring(className.lastIndexOf('.') + 1);
        String classNameNew = className + CLASS_NAME_SUFFIX;
        System.out.println("DataNucleus : JPA Criteria - " + className + " -> " + classNameNew);

        Map<String, TypeMirror> genericLookups = null;
        List<? extends TypeParameterElement> elTypeParams = el.getTypeParameters();
        for (TypeParameterElement elTypeParam : elTypeParams)
        {
            List<? extends TypeMirror> elTypeBounds = elTypeParam.getBounds();
            if (elTypeBounds != null && !elTypeBounds.isEmpty())
            {
                genericLookups = new HashMap<>();
                genericLookups.put(elTypeParam.toString(), elTypeBounds.get(0));
            }
        }

        TypeElement superEl = getPersistentSupertype(el);
        try
        {
            JavaFileObject javaFile = processingEnv.getFiler().createSourceFile(classNameNew);
            Writer w = javaFile.openWriter();
            try
            {
                w.append("package " + pkgName + ";\n");
                w.append("\n");
                w.append("import javax.annotation.processing.Generated;\n");
                w.append("\n");
                w.append("@Generated(value=\"" + this.getClass().getName() + "\")\n");
                w.append("public class " + classSimpleName + CLASS_NAME_SUFFIX);
                if (superEl != null)
                {
                    String superClassName = elementUtils.getBinaryName(superEl).toString();
                    w.append(" extends ").append(superClassName + CLASS_NAME_SUFFIX);
                }
                w.append("\n");
                w.append("{\n");

                List<? extends Element> members = getDefaultAccessMembers(el);
                if (members != null)
                {
                    Iterator<? extends Element> iter = members.iterator();
                    while (iter.hasNext())
                    {
                        Element member = iter.next();
                        boolean isTransient = false;
                        List<? extends AnnotationMirror> annots = member.getAnnotationMirrors();
                        if (annots != null)
                        {
                            Iterator<? extends AnnotationMirror> annotIter = annots.iterator();
                            while (annotIter.hasNext())
                            {
                                AnnotationMirror annot = annotIter.next();
                                if (annot.getAnnotationType().toString().equals(Transient.class.getName()))
                                {
                                    // Ignore this
                                    isTransient = true;
                                    break;
                                }
                            }
                        }

                        // Don't create static meta-model for STATIC or transient members
                        if (!member.getModifiers().contains(javax.lang.model.element.Modifier.STATIC) && !isTransient)
                        {
                            if (member.getKind() == ElementKind.FIELD ||
                                    (member.getKind() == ElementKind.METHOD && AnnotationProcessorUtils.isJavaBeanGetter((ExecutableElement) member)))
                            {
                                String memberName = AnnotationProcessorUtils.getMemberName(member);
                                w.append(CODE_INDENT).append("public static String " + memberName + " = \"" + memberName + "\";\n");
                            }
                        }
                    }
                }
                w.append("}\n");
                w.flush();
            }
            finally
            {
                w.close();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Method to find the next persistent supertype above this one.
     * @param element The element
     * @return Its next parent that is persistable (or null if no persistable predecessors)
     */
    public TypeElement getPersistentSupertype(TypeElement element)
    {
        TypeMirror superType = element.getSuperclass();
        if (superType == null || "java.lang.Object".equals(element.toString()))
        {
            return null;
        }
        TypeElement superElement = (TypeElement) processingEnv.getTypeUtils().asElement(superType);
        if (isR2DBCEntity(superElement))
        {
            return superElement;
        }
        return getPersistentSupertype(superElement);
    }

    public static List<? extends Element> getDefaultAccessMembers(TypeElement el)
    {
        Iterator<? extends Element> memberIter = el.getEnclosedElements().iterator();
        while (memberIter.hasNext())
        {
            Element member = memberIter.next();

            if (AnnotationProcessorUtils.isMethod(member))
            {
                ExecutableElement method = (ExecutableElement)member;
                if (AnnotationProcessorUtils.isJavaBeanGetter(method) || AnnotationProcessorUtils.isJavaBeanSetter(method))
                {
                    // Property
                    Iterator<? extends AnnotationMirror> annIter = member.getAnnotationMirrors().iterator();
                    while (annIter.hasNext())
                    {
                        AnnotationMirror ann = annIter.next();
                        String annTypeName = ann.getAnnotationType().toString();
                        if (annTypeName.startsWith("javax.persistence"))
                        {
                            return AnnotationProcessorUtils.getPropertyMembers(el);
                        }
                    }
                }
            }
        }
        return AnnotationProcessorUtils.getFieldMembers(el);
    }

    @Override
    public SourceVersion getSupportedSourceVersion()
    {
        return SourceVersion.latest();
    }
}