package vn.hesty.plugin.r2dbcmodelgen.anotation;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SuperTable {
}
