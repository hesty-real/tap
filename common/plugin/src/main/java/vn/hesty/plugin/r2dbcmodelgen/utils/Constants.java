package vn.hesty.plugin.r2dbcmodelgen.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Hardy Ferentschik
 */
public final class Constants {
    // we are trying to to reference jpa annotations directly
    public static final String ENTITY = "org.springframework.data.relational.core.mapping.Table";
    public static final String SUPER_ENTITY = "vn.hesty.plugin.r2dbcmodelgen.anotation.SuperTable";

    public static final List<String> BASIC_TYPES = new ArrayList<>();

    static {
        BASIC_TYPES.add( java.lang.String.class.getName() );
        BASIC_TYPES.add( java.lang.Boolean.class.getName() );
        BASIC_TYPES.add( java.lang.Byte.class.getName() );
        BASIC_TYPES.add( java.lang.Character.class.getName() );
        BASIC_TYPES.add( java.lang.Short.class.getName() );
        BASIC_TYPES.add( java.lang.Integer.class.getName() );
        BASIC_TYPES.add( java.lang.Long.class.getName() );
        BASIC_TYPES.add( java.lang.Float.class.getName() );
        BASIC_TYPES.add( java.lang.Double.class.getName() );
        BASIC_TYPES.add( java.math.BigInteger.class.getName() );
        BASIC_TYPES.add( java.math.BigDecimal.class.getName() );
        BASIC_TYPES.add( java.util.Date.class.getName() );
        BASIC_TYPES.add( java.util.Calendar.class.getName() );
        BASIC_TYPES.add( java.sql.Date.class.getName() );
        BASIC_TYPES.add( java.sql.Time.class.getName() );
        BASIC_TYPES.add( java.sql.Timestamp.class.getName() );
        BASIC_TYPES.add( java.sql.Blob.class.getName() );
    }

    public static final List<String> BASIC_ARRAY_TYPES = new ArrayList<>();

    static {
        BASIC_ARRAY_TYPES.add( java.lang.Character.class.getName() );
        BASIC_ARRAY_TYPES.add( java.lang.Byte.class.getName() );
    }

    public static final String PATH_SEPARATOR = "/";

    private Constants() {
    }
}
