package vn.hesty.plugin.r2dbcmodelgen.utils;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
public class TypeUtils {

    public static boolean containsAnnotation(Element element, String... annotations) {
        assert element != null;
        assert annotations != null;

        List<String> annotationClassNames = new ArrayList<>();
        Collections.addAll( annotationClassNames, annotations );

        List<? extends AnnotationMirror> annotationMirrors = element.getAnnotationMirrors();
        for ( AnnotationMirror mirror : annotationMirrors ) {
            if ( annotationClassNames.contains( mirror.getAnnotationType().toString() ) ) {
                return true;
            }
        }
        return false;
    }
}
