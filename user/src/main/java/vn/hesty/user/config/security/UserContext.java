package vn.hesty.user.config.security;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
public class UserContext implements Serializable {

    private static final long serialVersionUID = 1L;
    private UUID id;
    private String username;
    private String fullName;
}
