package vn.hesty.user.domain;

import lombok.*;
import net.lecousin.reactive.data.relational.annotations.ForeignKey;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.util.UUID;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "public.role_privilege")
public class RolePrivilege extends AuditingEntity{

    @ForeignKey
    private Role role;

    @ForeignKey
    private Privilege privilege;
}
