package vn.hesty.user.domain;

import lombok.*;
import net.lecousin.reactive.data.relational.annotations.ForeignKey;
import net.lecousin.reactive.data.relational.annotations.ForeignTable;
import net.lecousin.reactive.data.relational.annotations.GeneratedValue;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.MappedCollection;
import org.springframework.data.relational.core.mapping.Table;
import vn.hesty.user.constants.EGender;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "public.user")
public class User extends AuditingEntity{

    @Id
    @Column("id")
    private String id;

    @Column("username")
    private String username;

    @Column("phone")
    private String phone;

    @Column("email")
    private String email;

    @Column("full_name")
    private String fullName;

    @Column("gender")
    private EGender gender;

    @Column("s3_object_key")
    private String s3ObjectKey;

    @Column("s3_bucket")
    private String s3Bucket;

    @ForeignTable(joinKey = "user")
    private List<UserRole> userRoles;
}
