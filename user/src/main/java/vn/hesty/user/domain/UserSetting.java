package vn.hesty.user.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.util.UUID;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(value = "public.user_setting")
public class UserSetting extends AuditingEntity{

    @Id
    @Column("user_id")
    private String userId;
}
