package vn.hesty.user.repository;

import net.lecousin.reactive.data.relational.repository.LcR2dbcRepository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import vn.hesty.user.domain.User;
import vn.hesty.user.repository.specification.SpecificationRepository;

import java.util.UUID;

@Repository
public interface UserRepository extends LcR2dbcRepository<User, String>, SpecificationRepository<User> {

}
