package vn.hesty.user.repository;

import net.lecousin.reactive.data.relational.repository.LcR2dbcRepository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import vn.hesty.user.domain.User;

import java.util.UUID;

@Repository
public interface UserSettingRepository extends LcR2dbcRepository<User, String> {

}
