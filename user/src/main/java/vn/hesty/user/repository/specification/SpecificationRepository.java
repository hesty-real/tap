package vn.hesty.user.repository.specification;

import net.lecousin.reactive.data.relational.query.SelectQuery;
import org.springframework.data.domain.Pageable;
import org.springframework.data.relational.core.query.Query;
import reactor.core.publisher.Flux;

public interface SpecificationRepository<T> {
    Flux<T> findAll(SelectQuery<T> query, Pageable pageable);
}
