package vn.hesty.user.repository.specification.impl;

import net.lecousin.reactive.data.relational.LcReactiveDataRelationalClient;
import net.lecousin.reactive.data.relational.query.SelectQuery;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.data.relational.core.query.Query;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import vn.hesty.user.repository.specification.SpecificationRepository;

import java.lang.reflect.ParameterizedType;
import java.util.Optional;

@Component
public class SpecificationRepositoryImpl<T> implements SpecificationRepository<T> {

    private final LcReactiveDataRelationalClient lcClient;

    public SpecificationRepositoryImpl(LcReactiveDataRelationalClient lcClient) {
        this.lcClient = lcClient;
    }

    @Override
    public Flux<T> findAll(SelectQuery<T> query, Pageable pageable) {
        query.limit(pageable.getOffset(), pageable.getPageSize());
        Optional<Sort.Order> orderOptional = pageable.getSort().get().findFirst();
        if (orderOptional.isPresent()){
            Sort.Order order = orderOptional.get();
            query.orderBy(order.getProperty(),order.getDirection().isAscending()).execute(lcClient);
        }
        return query.execute(lcClient);
    }
}
