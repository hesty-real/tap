package vn.hesty.user.rest;

import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import vn.hesty.user.service.UserService;
import vn.hesty.user.service.criteria.UserCriteria;
import vn.hesty.user.service.dto.request.UserRequestDto;
import vn.hesty.user.service.dto.respond.UserRespondDto;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/user")
public class UserResource {

    private final UserService service;

    public UserResource(UserService service) {
        this.service = service;
    }

    @PostMapping
    public Mono<ResponseEntity<UserRespondDto>> create(@RequestBody UserRequestDto dto){
        //TODO load user context
        return service.create(null,dto)
                .map(userResponse -> ResponseEntity.status(HttpStatus.OK).body(userResponse))
                .defaultIfEmpty(ResponseEntity.badRequest().build());
    }

    @PutMapping
    public Mono<ResponseEntity<UserRespondDto>> update(@RequestBody UserRequestDto dto){
        //TODO load user context
        return service.update(null,dto)
                .map(userResponse -> ResponseEntity.status(HttpStatus.OK).body(userResponse))
                .defaultIfEmpty(ResponseEntity.badRequest().build());
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<UserRespondDto>> findById(@PathVariable("id") String id){
        //TODO load user context
        return service.findById(null,id)
                .map(userResponse -> ResponseEntity.status(HttpStatus.OK).body(userResponse))
                .defaultIfEmpty(ResponseEntity.badRequest().build());
    }

    @GetMapping("/find-all")
    public Flux<ResponseEntity<UserRespondDto>> findAll(UserCriteria criteria, Pageable pageable){
        //TODO load user context
        return service.findAll(null, criteria, pageable)
                .map(userResponse -> ResponseEntity.status(HttpStatus.OK).body(userResponse))
                .defaultIfEmpty(ResponseEntity.badRequest().build());
    }
}
