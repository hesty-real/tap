package vn.hesty.user.service;

import org.springframework.data.domain.Pageable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import vn.hesty.user.config.security.UserContext;
import vn.hesty.user.service.criteria.UserCriteria;
import vn.hesty.user.service.dto.request.UserRequestDto;
import vn.hesty.user.service.dto.respond.UserRespondDto;

import java.util.UUID;

public interface UserService {
    Mono<UserRespondDto> create(UserContext userContext, UserRequestDto dto);
    Mono<UserRespondDto> update(UserContext userContext, UserRequestDto dto);
    Mono<Void> delete(UserContext userContext, String id);
    Mono<UserRespondDto> findById(UserContext userContext, String id);
    Flux<UserRespondDto> findAll(UserContext userContext, UserCriteria criteria, Pageable pageable);
}
