package vn.hesty.user.service.criteria;

import lombok.Getter;
import lombok.Setter;
import vn.hesty.user.utils.filter.StringFilter;

import java.io.Serializable;

@Getter
@Setter
public class UserCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private StringFilter id;
    private StringFilter username;
    private StringFilter fullName;

    public UserCriteria(UserCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.username = other.username == null ? null : other.username.copy();
        this.fullName = other.fullName == null ? null : other.fullName.copy();
    }

    @Override
    public UserCriteria copy() {
        return new UserCriteria(this);
    }
}
