package vn.hesty.user.service.dto.message;

import lombok.*;

import java.io.Serializable;

@Setter
@Getter
@Builder
@NoArgsConstructor
@ToString
public class UserMessage implements Serializable {
    private static final long serialVersionUID = 1L;
}
