package vn.hesty.user.service.dto.request;

import lombok.*;
import vn.hesty.user.constants.EGender;

import java.io.Serializable;
import java.util.UUID;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserRequestDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String username;
    private String phone;
    private String email;
    private String fullName;
    private EGender gender;
}
