package vn.hesty.user.service.dto.respond;

import lombok.*;
import vn.hesty.user.constants.EGender;

import java.io.Serializable;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserRespondDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String username;
    private String phone;
    private String email;
    private String fullName;
    private EGender gender;
}
