package vn.hesty.user.service.impl;

import net.lecousin.reactive.data.relational.query.SelectQuery;
import net.lecousin.reactive.data.relational.query.criteria.Criteria;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import vn.hesty.user.config.security.UserContext;
import vn.hesty.user.domain.User;
import vn.hesty.user.domain.User_;
import vn.hesty.user.repository.UserRepository;
import vn.hesty.user.service.UserService;
import vn.hesty.user.service.criteria.UserCriteria;
import vn.hesty.user.service.dto.request.UserRequestDto;
import vn.hesty.user.service.dto.respond.UserRespondDto;
import vn.hesty.user.service.mapper.UserMapper;
import vn.hesty.user.utils.filter.Filter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

import static org.springframework.data.relational.core.query.Criteria.where;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository repository;
    private final UserMapper mapper;

    public UserServiceImpl(UserRepository repository, UserMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Override
    public Mono<UserRespondDto> create(UserContext userContext, UserRequestDto dto) {
        //check permission
        User user = User.builder().username(dto.getUsername())
                .id(UUID.randomUUID().toString())
                .email(dto.getEmail())
                .phone(dto.getPhone())
                .fullName(dto.getFullName())
                .gender(dto.getGender())
                .build();
        return repository.save(user).map(i->mapper.toDto(i));
    }

    @Override
    public Mono<UserRespondDto> update(UserContext userContext, UserRequestDto dto) {
        //check permission
        return repository.findById(dto.getId()).flatMap(i->{
            i.setFullName(dto.getFullName());
            i.setGender(dto.getGender());
            if (i.getEmail()==null) i.setEmail(dto.getEmail());
            if (i.getUsername()==null) i.setUsername(dto.getUsername());
            if (i.getPhone()==null) i.setPhone(dto.getPhone());
            return repository.save(i).map(j->mapper.toDto(j));
        });
    }

    @Override
    public Mono<Void> delete(UserContext userContext, String id) {
        //check permission
        return repository.findById(id)
                .flatMap(i->repository.delete(i));
    }

    @Override
    public Mono<UserRespondDto> findById(UserContext userContext, String id) {
        //check permission
        return repository.findById(id).map(i->mapper.toDto(i));
    }

    @Override
    public Flux<UserRespondDto> findAll(UserContext userContext, UserCriteria criteria, Pageable pageable) {
        SelectQuery<User> query = SelectQuery.from(User.class, "entity");
        List<Criteria> criteriaList = new ArrayList<>();
        if (criteria.getUsername()!=null) {
            criteriaList.add(build("entity",User_.username,criteria.getUsername()));
        }
        if (criteria.getFullName()!=null) {
            criteriaList.add(build("entity",User_.fullName,criteria.getFullName()));
        }
        if (criteriaList.size() > 1){
            Criteria criteria1=criteriaList.get(0);
            for (int i = 1; i<criteriaList.size();i++){
                criteria1.and(criteriaList.get(i));
            }
            query.where(criteria1);
        }
        return repository.findAll(query,pageable).map(i->mapper.toDto(i));
    }

    Criteria build(String entityName, String columnName, Filter filter){
        if (filter.getEquals() != null) {
            return Criteria.property(entityName, columnName).like(filter.getEquals());
        } else if (filter.getIn() != null) {
            return Criteria.property(entityName, columnName).in(filter.getIn());
        } else if (filter.getNotIn() != null) {
            return Criteria.property(entityName, columnName).notIn(filter.getNotIn());
        } else if (filter.getNotEquals() != null) {
            return Criteria.property(entityName, columnName).notLike(filter.getEquals());
        }
        return null;
    }

}
