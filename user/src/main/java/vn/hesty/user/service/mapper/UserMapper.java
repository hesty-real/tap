package vn.hesty.user.service.mapper;

import org.mapstruct.*;
import vn.hesty.user.domain.User;
import vn.hesty.user.service.dto.respond.UserRespondDto;

@Mapper(componentModel = "spring")
public abstract class UserMapper {

    @Named("getAvatar")
    @BeforeMapping
    protected void getAvatar(User user, @MappingTarget UserRespondDto dto) {
        if (user.getS3ObjectKey()!=null && !user.getS3ObjectKey().isEmpty()) {
            //Do something
        }
    }

    @BeanMapping(qualifiedByName= "getAvatar", ignoreByDefault = true)
    @Mapping(source = "id", target = "id")
    @Mapping(source = "phone", target = "phone")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "username", target = "username")
    @Mapping(source = "fullName", target = "fullName")
    @Mapping(source = "gender", target = "gender")
    public abstract UserRespondDto toDto(User user);
}
