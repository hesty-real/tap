CREATE TABLE public.privilege
(
    id BIGINT NOT NULL,
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    description text COLLATE pg_catalog."default",
    created_by character varying(36) COLLATE pg_catalog."default",
    created_at time without time zone,
    updated_by character varying(36) COLLATE pg_catalog."default",
    updated_at time without time zone,
    CONSTRAINT privilege_pkey PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public.privilege
    OWNER to postgres;