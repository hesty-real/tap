CREATE TABLE public.role_privilege
(
    role_id character varying(36) COLLATE pg_catalog."default" NOT NULL,
    privilege_id BIGINT NOT NULL,
    created_by character varying(36) COLLATE pg_catalog."default",
    created_at timestamp without time zone,
    updated_by character varying(36) COLLATE pg_catalog."default",
    updated_at timestamp without time zone,
    CONSTRAINT role_privilege_pkey PRIMARY KEY (role_id, privilege_id)
)

    TABLESPACE pg_default;

ALTER TABLE public.role_privilege
    OWNER to postgres;