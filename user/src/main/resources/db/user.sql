CREATE TABLE public."user"
(
    id character varying(36) COLLATE pg_catalog."default" NOT NULL,
    username character varying(255) COLLATE pg_catalog."default" NOT NULL,
    email character varying(255) COLLATE pg_catalog."default" NOT NULL,
    full_name character varying(255) COLLATE pg_catalog."default",
    phone character varying(255) COLLATE pg_catalog."default",
    gender character varying(50) COLLATE pg_catalog."default",
    s3_object_key character varying(255) COLLATE pg_catalog."default",
    s3_bucket character varying(255) COLLATE pg_catalog."default",
    created_by character varying(36) COLLATE pg_catalog."default",
    created_at timestamp without time zone,
    updated_by character varying(36) COLLATE pg_catalog."default",
    updated_at timestamp without time zone,
    CONSTRAINT user_pkey PRIMARY KEY (id)
)

    TABLESPACE pg_default;

ALTER TABLE public."user"
    OWNER to postgres;