CREATE TABLE public.user_role
(
    user_id character varying(36) COLLATE pg_catalog."default" NOT NULL,
    role_id character varying(36) COLLATE pg_catalog."default" NOT NULL,
    is_primary boolean NOT NULL,
    created_by character varying(36) COLLATE pg_catalog."default",
    created_at timestamp without time zone,
    updated_by character varying(36) COLLATE pg_catalog."default",
    updated_at timestamp without time zone,
    CONSTRAINT user_role_pkey PRIMARY KEY (user_id, role_id)
)

    TABLESPACE pg_default;

ALTER TABLE public.user_role
    OWNER to postgres;