CREATE TABLE public.user_setting
(
    user_id character varying(36) COLLATE pg_catalog."default" NOT NULL,
    created_by character varying(36) COLLATE pg_catalog."default",
    created_at time without time zone,
    updated_by character varying(36) COLLATE pg_catalog."default",
    updated_at time without time zone,
    CONSTRAINT user_setting_pkey PRIMARY KEY (user_id)
)

    TABLESPACE pg_default;

ALTER TABLE public.user_setting
    OWNER to postgres;